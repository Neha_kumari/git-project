public class OrthoPedician extends Doctor{
   
    public void treatPatient(){
        conductCTScan();
        conductXray();
    }
    public void conductCTScan(){
        System.out.println("Conducting CTScan");
    }

    public void conductXray(){
        System.out.println("Conducting X-Ray");
    }

}