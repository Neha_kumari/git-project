public abstract class BankApp{

    private String name;
    private int AccNo;
     public BankApp(String name, int AccNo){
        this.name= name;
        this.AccNo=AccNo;
     }

    public abstract void withdrawAmount(double amount);
    public abstract void depositAmount(double amount);

    public String toString(){
       return name+" "+AccNo;
    }
}