public class Current extends BankApp{
  
    public Current(String name, int AccNo) {
		super(name, AccNo);
    }

	public static double minBal=25000;
    
    @Override
    public void withdrawAmount(double amount){
    if(minBal<25000){
        System.out.println("Can not withdraw money ");
    }
    else{
        minBal= minBal-amount;
        System.out.println("The available balance is" +minBal);
    }
  }

  @Override
  public void depositAmount(double amount){
          minBal= minBal+amount;
          System.out.println("The available balance is" +minBal);
  }

}