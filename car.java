public class car{
    private String model;
    private String make;
    private int speed;
    private String color;

    public void accelerate (){
        speed++;
    }
    public void slowDown(){
        speed--;
    }

    public void halt(){
        speed =0;
    }
    
    public int getCurrentSpeed(){
        return speed;
    }

}